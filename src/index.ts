import * as fs from 'fs';
import path from 'path'


import PageFiles from "./files/page.files";
import Files from "./files";

let cwd = process.cwd();
let appRoot = path.resolve(cwd, './src/app');
let paths = {
    appRoot: appRoot,
    page: path.resolve(appRoot, './pages'),
    component: path.resolve(appRoot, './components'),
    service: path.resolve(appRoot, './services'),
    middlewares: path.resolve(appRoot, './middlewares')
}

export const generateFiles= async (fnm: string, ftype: string) => {
        let files = new Files(fnm, ftype);
        let format = files.generateFileFormat();
        // @ts-ignore
        console.log('Generating in ', paths[ftype]);
        console.log("Formatting: ",format)
    // @ts-ignore
        let filesPath = path.resolve(paths[ftype], `./${fnm}`);
    // @ts-ignore
        if (!fs.existsSync(paths[ftype])){
            // @ts-ignore
            fs.mkdirSync(paths[ftype]);
        }
        if(!fs.existsSync(filesPath)){
            fs.mkdirSync(filesPath);
            for (const filetype of format.filesToGenerate) {
                // @ts-ignore
                fs.writeFileSync(path.resolve(filesPath, `./${format.nameFormat}.${filetype}`), format[filetype+'_content']);
            }
        }
        else {
            console.log(`${fnm} Page Already Exists!`);
            return null;
        }

}

