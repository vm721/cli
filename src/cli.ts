import arg from 'arg';
import inquirer from "inquirer";
import { generateFiles } from './index'

const parseArgumentIntoOptions = (rawArgs: any): any => {
    const args = arg(
        {
            '--generate': Boolean,
            '--page': Boolean,
            '--component': Boolean,
            '--service': Boolean,
            '--middleware': Boolean,
            '-g': '--generate',
            '-p': '--page',
            '-c': '--component',
            '-s': '--service',
            '-m': '--middleware',
        },
        {
            argv: rawArgs.slice(2),
        }
    );
    return {
        generate: args['--generate'] || false,
        page: args['--page'] || false,
        component: args['--component'] || false,
        service: args['--service'] || false,
        middleware: args['--middleware'] || false,
        name: args._[0]

    }
}


 const promptForMissingOptions = async (options: any) => {
    let questions = [];
    let finalOptions = {...options};
    if(!options.generate){
        questions.push({
            type: 'list',
            name: 'File Type',
            message: 'What would you like to do?',
            choices: ['generate'],
            default: 'generate'
        });
    }
     let fileType: any= undefined;
     Object.keys(options).map((key) => {
         if(options[key] && key != 'generate'){
             if(!fileType){
                 fileType = key;
             }
         }
     });
     options.filetype = fileType;
     if(!options.filetype){
         questions.push({
             type: 'list',
             name: 'filetype',
             message: 'Please Select File Type',
             choices: ['page', 'component', 'service', 'middleware'],
             default: 'component'
         });
     }
     if(!options.name){
         questions.push({
             name: 'name',
             message: 'Please Enter file name',
         });
     }

    const answers = await inquirer.prompt(questions);

     options.generate = options.generate || answers.generate
     options.filetype = options.filetype || answers.filetype
     options.name = options.name || answers.name
     options.name = options.name.toLowerCase()

     return {
         ...options
     };
}


export const cli = async (args: String[]) => {
    console.log("Iam VM721", args);
    let options = await parseArgumentIntoOptions(args);
    console.log("Arguments Parsed as", options);
    let finalOptions = await promptForMissingOptions(options);
    console.log("Final options:", finalOptions);
    generateFiles(options.name, options.filetype);
}
