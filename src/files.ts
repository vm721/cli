
export default class Files
{
    nameFormat = 'default.file';
    name= '';
    type= '';
    filesToGenerate = {
        page: ["ts", "scss", "html"],
        component: ["ts", "scss", "html"],
        service: ["ts"],
        middleware: ["ts"]
    };
    fileCoreImports = {
        page: "Component, OnInit",
        component: "Component, OnInit",
        service: "Injectable",
        middleware: "Injectable"
    }
    fileDecorators = {
        page: (nm: string) => `@Component({
  selector: 'app-${nm}',
  templateUrl: './${this.nameFormat}.html',
  styleUrls: ['./${this.nameFormat}.scss']
})`,
        component: (nm: string) => `@Component({
  selector: 'app-${nm}',
  templateUrl: './${this.nameFormat}.html',
  styleUrls: ['./${this.nameFormat}.scss']
})`,
        service: (nm: string) => `@Injectable({
  providedIn: 'root'
})`,
        middleware: (nm: string) => `@Injectable({
  providedIn: 'root'
})`
    }


    constructor(nm: string, ftyp: string) {
        this.name = nm;
        this.type = ftyp;
        this.nameFormat=`${nm}.${ftyp}`;

    }
    generateFileFormat = () => {
        let format = {
            nameFormat: this.nameFormat,
            // @ts-ignore;
            filesToGenerate: [...this.filesToGenerate[this.type]],
        }
        for (const ftype of format.filesToGenerate){
            // @ts-ignore;
            format[ftype+'_content']= this.generators[ftype].call(this, this.name, this.type);
        }
        return format

    }

    generators = {
        ts: (nm: string, type: string) => {
            let name = nm.slice(0,1).toUpperCase() + nm.slice(1);
            let typ = type.slice(0,1).toUpperCase() + type.slice(1);
            let content = '';
            // @ts-ignore;
            content += `import { ${ this.fileCoreImports[type] } } from '@angular/core';\n\n`;
            // @ts-ignore;
            content += `${this.fileDecorators[type].call(this, nm)}\nexport class ${name}${typ}`;
            content += (type == 'page' || type == 'component'? ` implements OnInit ` : '') ;
            content +=`{\n\nconstructor() { }\n\n`;
            content += (type == 'page' || type == 'component'? `\nngOnInit(): void { }\n` : '') ;
            content += `}\n`;
            return content;


        },
        scss: (nm: string, type: string) => {
            return ``
        },
        html: (nm: string, type: string) => {
            return `<h3>${nm} ${type} works!</h3>`
        }
    }

}
