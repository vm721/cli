
export default class PageFiles
{
    nameFormat = 'default.page'
    scssFormat = ''
    tsFormat = ''
    htmlFormat= ''
    constructor(nm: string) {

        this.nameFormat=`${nm}.page`;
        this.tsFormat = this.generateEmptyTS(nm, '');
        this.scssFormat = this.generateEmptySCSS(nm);
        this.htmlFormat = this.generateEmptyHTML(nm)
    }
    generatePageFileFormat = () => {
        return {
            nameFormat: this.nameFormat,
            filesToGenerate: ["ts", "scss", "html"],
            ts_content: this.tsFormat,
            scss_content: this.scssFormat,
            html_content: this.htmlFormat
        }

    }

    generateEmptyTS(nm: string, type: string){
        let name = nm.slice(0,1).toUpperCase() + nm.slice(1);
        return `
import { Component, OnInit } from '@angular/core';\n

@Component({
  selector: 'app-${nm}',
  templateUrl: './${this.nameFormat}.html',
  styleUrls: ['./${this.nameFormat}.scss']
})
export class ${name}Page implements OnInit {

  constructor() { }

  ngOnInit(): void { }
}
`

    }

    generateEmptySCSS(nm: string){
        return ``
    }
    generateEmptyHTML(nm: string){
        return `<h3>${nm} Page works!</h3>`
    }
}
